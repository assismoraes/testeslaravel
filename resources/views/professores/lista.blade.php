@foreach($professores as $professor)
    <h3>{{ $professor->nome }}</h3>
    @foreach($professor->disciplinas as $disciplina)
        <h5> {{ $disciplina->descricao }} </h5>
    @endforeach
@endforeach

<hr>
@foreach($disciplinas as $disciplina)
    <h3>{{ $disciplina->descricao }}</h3>
    @foreach($disciplina->professores as $professor)
        <h5> {{ $professor->nome }} </h5>
    @endforeach
@endforeach

