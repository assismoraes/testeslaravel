@foreach($noticias as $noticia)
    <h3>{{ $noticia->corpo }}</h3>
    <h5>Categoria: {{ $noticia->categoria->descricao }}</h5>
@endforeach

<hr>

@foreach($categorias as $categoria)
    <h3>{{ $categoria->descricao }}</h3>
    @foreach($categoria->noticias as $noticia)
        <h5>{{ $noticia->corpo }}<h5>
    @endforeach
@endforeach