<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessorDisciplina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disciplina_professor', function (Blueprint $table) {
            $table->integer('professor_id')->unsigned();
            $table->integer('disciplina_id')->unsigned();

            $table->primary(['professor_id', 'disciplina_id']);

            $table->foreign('professor_id')->references('id')->on('professors')
                ->onUpdated('cascade')
                ->onDelete('cascade');

            $table->foreign('disciplina_id')->references('id')->on('disciplinas')
                ->onUpdated('cascade')
                ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professor_disciplina');
    }
}
