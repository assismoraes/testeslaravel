<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    public function disciplinas() {
        return $this->belongsToMany('App\Models\Disciplina');
    }
}
