<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model
{
    protected $guarded = [];

    public function professores() {
        return $this->belongsToMany('App\Models\Professor');
    }
}
