<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Professor;
use App\Models\Disciplina;

class ProfessorController extends Controller
{
    
    public function lista() {
        $disciplinas = Disciplina::all();
        $professores = Professor::all();
        //$d = Disciplina::create(['descricao' => 'Nova 1']);
        //$p = Professor::find(1)->disciplinas()->save($d);
        return view('professores.lista', compact(['professores', 'disciplinas']));
    }

}
