<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;
use App\Models\Categoria;

class NoticiaController extends Controller
{
    
            public function lista() {

                $cat = Categoria::find(3);
                $not = Noticia::find(1);
                $not->categoria()->associate($cat);
                $not->save();

                $noticias = Noticia::all();
                $categorias = Categoria::all();
                
                return view('noticias.lista', compact(['noticias', 'categorias']));
            }


}
